\chapter{Materials and Methods}

In this section we explain the workflow we followed to implement the selected algorithms. To train and test our classifiers, open source datasets have been chosen. Concerning the first case study, an explanation of software and hardware implementation will be provided. Regarding the second one, we will illustrate the software development of the Convolutional Neural Network and the training process. %Finally, the obtained results will be discussed and further improvements proposed.


\section{Dataset}

First of all, we looked for suitable and labelled databases for our Supervised Learning purpose. The presence of labels and annotation methods are fundamental to perform a correct training of the classification algorithms. For each case study we evaluated the available datasets on the basis of the most critical parameters. \newline

\paragraph{EMG Signals} \mbox{}\\
Regarding the first case study, the gesture recognition, we leveraged an open source database \cite{khushaba2012electromyogram}. 
The EMG signal datasets can vary according to the number of channels, i.e. the number of electrodes used to register the signal. Also their quality, the kind of movements measured and the filtering after acquisition are discriminatory parameters.\newline
The database we decided to analyse was acquired with eight subjects (six males and two females), aged between 20-35 years. The subjects were all normally limbed with no neurological or muscular disorders and were seated on an armchair, with their arm supported and fixed at one position. For each of them, 15 movements have been acquired for three times.\newline
A 12-bit analogue-to-digital converter was used to sample the signal at 4000 Hz and the EMG signals were then bandpass filtered between 20-450 Hz with a notch filter implemented to remove the 50 Hz line interference.\newline
Using the NMF algorithm, we reduced this dataset into two smaller matrices, and we classified the gestures using the time invariant coefficient. The matrix of the time invariant coefficient is $8$x$8$x$640$, which we arranged in a $64$x$640$ because we considered each $8$x$8$ matrix as a vector of $64$ independent features. As a consequence of the differences between each subject, every person will have different training parameters. In other words, each patient will have a personalized classification algorithm, different from any other. Thus, at the beginning, we worked with signals of just one person.\newline
Since three different signals of each movement were available, we decided to use 2 of them to train our algorithm and the third to verify the prediction accuracy. Furthermore, we decided to classify 5 movements (Thumb (T), Thumb-Index (T-I), Middle-Ring (M-R), Middle-Ring-Little (M-RL) and hand close (HC)), because they are the most significant and used in everyday life.\newline

\paragraph{Retinal Images from Fundus Camera} \mbox{}\\
Speaking about our second case of study, we searched for databases with a lesion-wise approach, so that we would have been able to obtain the precise coordinates of every lesion in the image and not the overall grading of the image. The main characteristic we analysed for Diabetic Retinopathy datasets are listed below in the table.

 \begin{table} [H]
 \centering
 \begin{tabular}{c|c|c|c}
 
 \toprule
  
& \textbf {ROC\cite{niemeijer2010retinopathy}} & \textbf{ e-ophtha\cite{decenciere2013teleophta}}  & \textbf{ DIARETB1\cite{kauppi2007diaretdb1}} \\ 
\midrule
\textbf{Number of Images} & 13 + 37  &  234+149 & 5 + 84  \\ \hline
\textbf{Image quality} & Challenging   & Optimal  & Good \\ \hline
\textbf{Annotations} & Coordinates (x,y, radius)   & Binary masks & Binary masks \\ \hline
\textbf{Evaluation} & FROC curve   & Not available & Matlab script \\ \hline


\end{tabular}
\caption{Comparison between datasets for DR lesion recognition. The number of images line includes healthy images plus unhealthy images }
 \label{tab:dataset}
\end{table}

What we noticed since the beginning was that all the considered datasets contained a small number of images. Therefore, all of them could be affected by overfitting issues and difficulties when trying to generalize results to other independent fundus camera images. The chosen dataset was ROC (Retinopathy Online Challenge) as it is the most widespread in the State of the Art, having a very punctual evaluation methodology. However, before the final implementation of our algorithm we could not actually know whether the selected choice would lead to valuable results.  

\section{Case Study 1: Gesture Recognition through SVM}

In this section we will explain the software and hardware implementation of SVM. To implement SVM classifier architecture on the hardware, we performed the training of the SVM off-line on software using Matlab. Then we used the extracted parameters to implement the tests on hardware. %For the hardware implementation we used Vivado to generate HDL design. 

\subsection{Software Implementation}
We implemented the SVM using MATLAB. The first step was to understand which type of SVM was the best in terms of accuracy. As explained before, there are different kind of kernels. We used the MATLAB Classification Learner App to train SVM using different kernels and determined their accuracy. The one that estimated the highest score was the linear SVM, with more than 95\% of training accuracy. For this reason we decided to improve Linear SVM.
The second step was to analyze the signal and understand how to improve it. Using another MATLAB tool, the Signal Analyzer, we noticed that there were many peaks which distorted the signal. These distortions were due to the initialization of the NMF algorithm. Therefore, we removed the first 10 observations of each movement from the dataset and as a consequence the accuracy increased. This kind of distortions will not be present in a real world application.
\\To train the classifier we decided to use two different measurements for each movement, in order to have some variability of the signal and avoid problems of either overfitting or underfitting. Overfitting occurs when a function is too closely fit to a limited set of data points, while underfitting occurs when the model or the algorithm does not fit the data well enough and can't capture the underlying trend of the data. To avoid overfitting it was also used a 5-fold cross-validation. The cross validation is a model validation technique for assessing how the results of a statistical analysis will generalize to an independent data set. The goal of cross validation is to define a dataset to test the model in the training phase (i.e. the validation dataset), in order to limit problems like overfitting \cite{kohavi1995study}.

We decided to implement the training algorithm using ECOC (error-correcting output codes). Moreover, we scaled the input data subtracting the mean and partitioning for the standard deviation $(x- \mu)/(\sigma)$. This standardization was necessary to give the observations the properties of a standard normal distribution with $\mu=0$ and $\sigma=1$. The standardization of the features is a general requirement for many machine learning algorithms and could determine a better accuracy, because there is lower variability. 
Considering that we used a one-vs-one strategy for the multi-class classification, the output structure comprised $\frac{(n)(n-1)}{2}$ binary learners, one classifier for each pair of labels. In the figure \ref{fig:codmat}, each movement corresponds to a unique string while ${h_1,h_2,...h_{10}}$ are the classifiers. In this case $N=5$ so $n = \frac{(5)(5-1)}{2} =10$. 

\begin{figure}[htbp]
	\centering
	\includegraphics[scale=0.4]{Pictures/Immaginecodingmatrix.png}
	\caption{Coding matrix for 5 movements.}
	\label{fig:codmat}
\end{figure}
The binary learners must be used in sequence. The first test is between the first movement and the second one. If the binary learner recognize the movement as the first one, the output of the decision function will be $1$, otherwise it will be $-1$. Then the first movement is compared to the third one and exactly the same process is repeated for every movement. If the first four binary learners have given as output always $1$, the input will be labelled with the first movement and a new input will be classified. If it is not predicted the first movement, the prediction goes on. Similarly, the second movement will be predicted if the first binary learner gave $-1$ as output and $1$ from the 5th, 6th and 7th binary learners. Therefore, studying the coding matrix is possible to understand which score must be given to code for each class.\newline
The function ECOC gives as output a struct in which weights and bias of every binary learners are saved. We extracted all the parameters needed in the prediction phase to create our prediction algorithm. We used the mean and the standard deviation to standardize the data. Then, we based our prediction function on the theory of SVM. The standardized input is then linearly combined with the weights and summed with the bias of each binary learner. The output of the decision function represents the predicted class.


\subsection{Hardware Implementation}

For the hardware implementation, we used Vivado Design Suite.\newline
First, we used Vivado HLS to generate SVM core in VHDL. At this stage, we have been implemented only the prediction algorithm previously described. 
In particular we exported the parameters estimated on MATLAB during the training phase. Since they are fixed, they have been implemented  in the block RAM of the board for fast retrieval. Another expedient for reducing the utilization of resources, has been used for the observations in input. Considering that the operations on the different samples of the input vector are independent from one another, they have been streamed one by one into the core. Each sample is then processed in parallel for each of the ten learners, allowing for a much lower computational cost. For this reason we can use an instruction pipelining.
Pipelining is an implementation technique where multiple instructions are overlapped in execution. With a pipeline, a computer processor divide an instruction set in stages and each stage completes a part of an instruction in parallel. The stages are connected one to the next to form a pipe of instructions. Consequently
the instructions enter at one end, progress through the stages, and exit at the other end and they will be completed.
Without a pipeline, a computer processor gets the first instruction from memory, performs the operation it calls for, and then goes to get the next instruction from memory, and so forth.
\newline
The hardware core receives one of the 64 feature at a time and feeds it to a pipeline with II=5. The pipeline computes the results of all the ten learners and, when all the features are processed, the result is compared with a look-up table containing the coding
matrix.
\newline
Before discussing about implementation hardware data, we would like to explain what are our most important FPGA's resources.
\newline

%Every FPGA chip is composed of a finite number of configurable logic blocks(CLBs) connected via programmable interconnects, as said before. The configuration of the CLBs and interconnections implement the logic designed inside the device. Nowadays, a CLB usually consists of multiple look-up tables (LUT),flip-flops and MUX.  Look-up tables are how your logic actually gets implemented. In particular, a LUT stores a predefined list of outputs for every combination of inputs and what makes a LUT powerful is that you can program what the output should be for every single possible input. Each LUT's output can be optionally connected to a flip-flop. Furthermore, modern FPGAs contain additional components, such as random access memory blocks (BRAMs) as local memory and digital signal processor blocks (DSP blocks) as efficient computation units. All this elements work together to create a very flexible device. \newline

In the following table, we reported the most important data about resource usage.
Since we leverage on the NMF algorithm for the factorization, we had to take in consideration also the utilization of this algorithm. The resource consumption for both the NMF and SVM is depicted in \tablename~\ref{tab:hw_res}. The NMF takes up most of the resources, while the SVM can be implemented in a highly parallel way using a low amount of components. It must be stated that resources are not exploited up to the 100\% because future implementations will integrate other processes, like signal acquisition and motor controllers.

\begin{table}[h!tb]
\centering
\caption{Occupation of resources on the FPGA of the operational cores.}
\label{tab:hw_res}
	\begin{tabular}{lcccc}
	\toprule
	\textbf{Operation} & \textbf{BRAM}[\%] & \textbf{DSP}[\%] & \textbf{FF}[\%] & \textbf{LUT}[\%]\\
	\midrule
	NMF & 51 & 59 & 32 & 57 \\
	SVM & 14 & 8 & 9 & 16 \\
	\midrule
	\textbf{Total} & 65 & 67 & 41 & 73\\
	\bottomrule
	\end{tabular}
\end{table}

Occupation of resources on the FPGA is linked to power consumption. For example, the reduction of power consumption is a crucial point in order to produce prostheses with longer autonomy and smaller, lighter, batteries.\newline
This result demonstrates that it is possible to achieve low power consumption without compromising on accuracy.
\newline 
After successfully generating our hardware design by Vivado IP integrator, the next step has been run it in hardware by programming the FPGA device and debugging the design in-system. This is possible by VIVADO and Software Development Kit(SDK).
The hardware programming phase is broken into two steps:
\begin{itemize} 
\item Generating the bitstream data programming file from the implemented design.
\item Connecting to hardware and downloading the programming file to the target FPGA
device.
\end{itemize}
As a result, the board has been programmed.



\section{Case Study 2: Diabetic Retinopathy detection through CNN}


In this section a detailed description of the implemented Deep Learning algorithm for fundus camera images segmentation will be provided, as well as the overview on the most important tools and programming languages adopted for its implementation. The section will illustrate both the classification general procedure to obtain a segmented image and the CNN training process adopted in order to provide an effective classification.

\subsection{Dataset elaboration} 

Retinopathy Online Challenge dataset is composed by 50 images in jpeg format, of which 22 images have 768x576 resolution (\textit{width x height}), 3 images have 1058x1061 resolution and 25 images have 1398x1383 resolution. The database is quite heterogeneous as images are acquired through different cameras and are precisely annotated by four trained ophtalmologists. According to general CNN training procedure, we divided ROC dataset in three parts: Training set ( 60 \%), Cross-Validation set (30 \%) and Test set (10 \%). Every image had a different number of microaneurysm consequently, while partitioning the dataset, we tried to obtain an homogeneous distribution of characteristics between the three sets. As images had a high resolution, this could lead to a too high number of data to be processed, therefore we decided to resize images according to Table \ref{tab:dataset} . We could not go further with image resizing for the reason that microaneurysms are very small with the result that they could disappear from fundus camera pictures.\newline

 \begin{table} [H]
 \centering
 \begin{tabular}{c|c|c}
 
 \toprule
\textbf {Number of Images} & \textbf{ Original Dimension}  & \textbf{ Resized dimension} \\ 

\midrule
22 & 768x576 & No resizing \\ 
3 & 1058x1061 & 576x576\\
3 & 1398x1383 & 576x576 \\ 
\bottomrule


\end{tabular}
\caption{Image dimension before and after resizing}
 \label{tab:dataset}
\end{table}

\subsection{Software Implementation}

\paragraph{Classification procedure} \mbox{}\\
The aim of this procedure is to obtain a microaneurism map in which healthy pixels are represented by black colour and microaneurisms are represented by white colour. Once this map is obtained, on the basis of the number of microaneurism, it is possible to establish wherever the patient's situation needs to be further explored.\newline

The considered algorithm is composed by three main steps: Pre-processing, Convolutional Neural Network application and Post-processing, which are graphically depicted in Figure \ref{fig:algonn}. In order to perform the fundus camera image segmentation with a pixel-wise approach we extract a window around each pixel of the considered image; all the extracted windows are then classified through the CNN in order to detect whether they represent an healthy portion of the image or a microaneurism. After that we plot on the microaneurism map a black or a white point, in correspondence of the central pixel of each extracted window, according to the output of our classifier network. All the details about each step will be provided hereafter.

\begin{figure}[H]
\includegraphics[scale=0.42]{Pictures/algo}
\centering
\caption{General overview of segmentation algorithm composed by: Pre-processing and window extraction, Convolutional Neural Network application, Post-Processing }
\label{fig:algonn}
\end{figure}

\begin{enumerate}

\item{Pre-processing: image parsing}

As seen before, in this step we extract a patch around each image pixel; the size of the selected window is a critical issue to deal with, as too large window could bring the CNN not to recognise the important classification features, while too small windows could cause the microaneurism not to be totally contained in the patch. Furthermore large windows are computationally heavy to be classified, whereas smaller ones are faster. On the basis of this consideration and additionally knowing that the maximum dimension of the microaneurism radius in ROC dataset corresponds to 10 pixels (i.e. a diameter of 20 pixels), we decided to consider a 51x51 window for our algorithm.\newline
 Python programming language was used for this part of implementation, for the reason that it is a high level language, object oriented, suitable for fast development of applications. Moreover it offers a large number of supported libraries, among which we leveraged OpenCV. OpenCV is a multi-language open source library, focused on image processing and elaboration; in particular in this phase we exploited the proper functions to read and write image files.
 
 \item{Convolutional Neural Network}
 
 Each obtained window is then classified using a Convolutional Neural Network. Our proposed network takes inspiration from a previous work dealing with hemorrages detection in fundus camera images \cite{van2016fast} and from VGG net, a particular kind of CNN using small kernel size. The main parameters of the network are numerically presented in Table \ref{tab:ourcnn} and graphically represented in Figure \ref{fig:cnnimple}.
 The CNN is composed by four convolutional layers and three max pooling layers followed by ReLU activation function. For the last layer we choose a Softmax classifier, which converts output network scores in probabilities. 
 
 %\vspace{35pt}
 
 \begin{figure}[H]
\includegraphics[scale=0.4]{Pictures/cnn}
\centering
\caption{Implemented Convolutional Neural Network structure}
\label{fig:cnnimple}
\end{figure}
 
 
 \begin{table} [H]
 \centering
 \begin{tabular}{p{0.1\textwidth}p{0.4\textwidth}p{0.25\textwidth}p{0.25\textwidth}}

\toprule
\textbf {Layer} & \textbf{\centering Input size\\ \centering (\textit{width x height x depth)}}  & \textbf{Operation } & \textbf{Kernel, Stride} \\ 
\midrule
1 & 51x51x3 & convolution &  3x3, 1 \\ 
2 & 49x49x8 & pooling &  3x3, 2 \\ 
3 & 24x24x8 & convolution &  3x3, 1 \\ 
4 & 22x22x16 & pooling &  2x2, 2 \\ 
5 & 11x11x16 & convolution &  3x3, 1 \\ 
6 & 9x9x32 & pooling &  3x3, 2 \\ 
7 & 4x41x32 & convolution &  3x3, 1 \\ 
8 & 2x2x32 & linear &  \\ 
9 & TbD & softmax & 2 classes\\ 
\bottomrule

\end{tabular}
\caption{Implemented Convolutional Neural Network details}
 \label{tab:ourcnn}
\end{table} 



\end{enumerate}

\paragraph{Training procedure} \mbox{}\\

In order to obtain a network able to classify our images, we trained the algorithm using 12.000 healthy windows extracted from healthy images and 12.000 microaneurism windows extracted from unhealthy images, both belonging to training part of ROC dataset. For training purpose, we exploited Stochastic Gradient Descent (SGD), an algorithm suitable for function minimization, as it is the most diffused and straightforward way to train Convolutional Neural Networks. In particular in the case of Softmax classifier the algorithm tries to minimize the Cross-Entropy figure of merit, which represents the difference between the desired probability distribution and the predicted one and is calculated as follows: 

\begin{equation}
 L_{i}=-ln   \frac{e^{f_{y_i}}}{\sum_j e^{f_{y_j}}}
\end{equation}

The SGD modifies the value of network weights at each training step, so that it minimizes the N-dimensional function obtained considering $N$ weights and biases as independent variables and Cross-Entropy as dependent variable.\newline

We choose to implement and train the network leveraging an open source Machine Learning framework: Tensorflow \cite{tf}. This library was originally developed by Google's engineers working in the Artificial Intelligence field and is wrapped into a more user-friendly Python API. The computational pattern is based on graphs representing data flow; they are composed by nodes which correspond to mathematical operations performed on multidimensional arrays (tensors). With this approach it is possible to deploy computation to CPUs and GPUs and to visualize results in a web interface named TensorBoard. Tensorboard has been our main tool used to monitor our CNN while training and fine-tuning its parameters. As training tool, we exploited the computational power of a Ge Force GTX GPU in order to speed up the entire process.
The Table \ref{fig:train} represents the parameters we set for training procedure.

 \begin{table} [H]
 \centering
 \begin{tabular}{c|c}

\toprule
\textbf {Parameter} & \textbf{Value} \\

\midrule
Training steps & 9500 \\ 
Batch dimension & 150 \\ 
Input size & 51x51 \\ 
Healthy windows & 12000 \\ 
Microaneurism windows & 12000 \\ 
Output classes  & 2 \\ 
%Data augmentation & Rotation, Random contrast, Random brightness \\ \hline
Initial learning rate & 0.09\\
Weight initialization & Normal distribution (mean= 0, stdev=\begin{math}5*10^{-2}\end{math}) \\ 
Biases initialization & 0 \\ 
\bottomrule

\end{tabular}
\caption{Convolutional Neural Network Training parameters }
 \label{tab:train}
\end{table} 


The considered parameters were established after trying different configuration and resulted to be the most stable in terms of network training, i.e. the error converges to a value proximal to zero, without presenting peaks. The graphic in Figure \ref{fig:train}is extracted from Tensorboard and represents the obtained error (Cross-Entropy) in respect to the number of training steps; the initial error is 0.76, which then decreases to 0.03 after 9500 training steps. 

\begin{figure}[H]
\includegraphics[scale=0.45]{Pictures/train}
\centering
\caption{The graphic represents the Cross-Entropy loss error in respect to the number of steps. At 9500 steps the training is completed.}
\label{fig:train}
\end{figure}

The number of images in each training batch was experimentally chosen according to a trade-off between  higher values, which imply a too slow training process, and smaller values, corresponding to very oscillating error curve. The learning rate (i.e. a parameter indicating how much network weights are modified after each training step) was fixed at 0.09, as lower learning rate could cause a too slow training, whereas higher values could imply an inefficient training with spikes and instability.
Concerning data augmentation we decided to implement three different rotations for images using Python and to leverage Tensorflow in order to obtain random bright and random contrasted images during training.








