\chapter{State of Art and Related Work}

We decided to analyse two different case studies. We tried to develop each of them in a parallel workflow, even though their implementation are extremely different, as we deal with different types of signal.\\
 The first case study is related to Gesture Recognition: we analysed the sEMG signal and developed an algorithm to classify the signal and identify the correct movement. Concerning the second case study, we deal with images; more specifically, we developed a classification algorithm to detect retinal microaneurysms caused by Diabetes. In both cases we developed a software implementation, while the hardware implementation was developed only for the first case study, as the second one required a more sophisticated software implementation. In the following paragraphs a brief overview on the State of Art of the two case studies will be provided, as well as some possible implementation of these algorithms on the FPGA device. In the following paragraphs we will also give reasons why one of the previously mentioned algorithm was not suitable for our purpose.


\section{Gesture Recognition through Support Vector Machine}
\label{sec:gesture}

Gestures are expressive, meaningful body motions involving physical movements of the fingers, hands, arms, head, face or body with the intent of conveying meaningful information or interacting with the environment.
%\begin{enumerate}
%\item conveying meaningful information or 
%\item interacting with the environment.
%\end{enumerate}
They constitute one interesting small subspace of possible human motions. A gesture may also be perceived by the environment as a compression technique for the information to be transmitted elsewhere and subsequently reconstructed by the receiver\cite{mitra2007gesture}. \newline
We decided to focus our attention on gestures that are related to electromyography (EMG), which could be useful for many applications like prosthesis, rehabilitation therapy, automated remote control etc. Electromyography signal is an electrical manifestation of neuromuscular activation associated with a contracting muscle. It is an exceedingly complicated signal which is affected by the anatomical and physiological properties of muscles, the control scheme of the peripheral nervous system, as well as the characteristics of the instrumentation that is used to detect and observe it. Most of the relationships between the EMG signal and the properties of a contracting muscle evolve randomly\cite{rekhi2009multi}. Therefore, its application to control prosthetic limbs (the field we are more interested in), that can restore some or all of the lost motor functions of amputees, has presented a great challenge due to the complexity of the EMG signal. Feature extraction and function classification is the key in processing and analysing this type of signal. \newline

The utilization of factorization algorithms to analyse muscle activity recorded during motor behaviors has made it possible to decompose myoelectric activation patterns into their building blocks, thereby revealing the modular architecture of the motor system \cite{cheung2012muscle}. In the last few years, many scientists have pursued this approach, \cite{tresch2002coordination}, \cite{bizzi2008combining}, to shed light on how the motor system circumvents the need to control its large number of degrees of freedom through a flexible combination of motor modules in both animals and humans.  Accordingly to many papers and researches, the factorization analysis that we have used, models motor modules as groups of muscles activated together (muscle synergies). Each muscle synergy represents a time-invariant profile of activation across muscles, activated by a time-varying coefficient. When individual synergies, scaled by their coefficients, are summed together, the muscle activation patterns, recorded during movements as electromyographic signals (EMGs), are faithfully reconstructed (Figure \ref{fig:syn}).\newline

\begin{figure}[htbp]
	\centering
	\includegraphics[scale=0.8]{Pictures/musclesyn.jpg}
	\caption{Muscle synergies.}
	\label{fig:syn}
\end{figure}

We used the non-negative matrix factorization (NMF) algorithm \cite{lee1999learning} in order to extract from the EMGs the number of muscle synergies necessary for an EMG reconstruction. This is a powerful algorithm that allows to retrieve the two elements that, linearly combined, codify for the EMG. The resulting processed signal returned two matrices: one with the time-invariant coefficient ($8$x$8$x$640$), and one with the time-varying profile ($8$x$250$x$640$). We verified that only the time invariant coefficent was needed to detect the movements and this matrix became the base of our classification algorithm. In order to understand which type of algorithm was well suited for our purpose,  we studied the State of Art and we found that many works have leveraged SVM with a direct application to the classification of EMG signals, \cite{yoshikawa2006real}, \cite{oskoei2008support} and \cite{liu2007recognition}. 
The most notable implementations have reported accuracy rates above 90\%, the highest being 96.76\%\cite{liu2007recognition}. Yoshikawa et al. \cite{yoshikawa2006real} proposed a real time hand motion estimation method using EMG signals. To accomplish this, they used four surface electrodes placed on a patient's forearm to distinguish between seven hand motions: at rest, opening of the hand, closing of the hand, pronation, supination as well as wrist flexion and extension. The EMG signals processing and classification was performed on a PC. In order to perform the classification, they used an SVM with input vectors composed by integrated EMG signal. With this set of features, the classification rate ranged from approximately 87\% to 92\%.\newline

Another important work is the one explained by Oskoei et al. \cite{oskoei2008support}, which compared the classification ability of SVMs with the linear discriminant analysis (LDA) and multilayer perceptron (MLP) neural networks in myoelectric control of limb motions (Figure \ref{fig:confronto}). Feature selection is an essential stage, especially in myoelectric control design, but computational load should also be considered in real-time applications. The involved features in the time domain are: mean absolute value (MAV), root mean square (RMS), wave form length (WL), zero crossings (ZCs), slope sign changes (SSC) and autoregressive coefficients of order 6 (AR6). The features of each channel were extracted from segments with various lengths and then concatenated together and fed into a classifier.

\begin{figure}[htbp]
	\centering
	\includegraphics[scale=0.6]{Pictures/confronto.jpg}
	\caption{The following experiment compared the accuracy of the SVM with LDA and MLP. The classifiers were examined one-by-one over a single feature MAV, multi-feature MAV+WL+SSC+ZC, and multi-feature RMS + AR6, individually. The first four items of the graph are SVM-based classifiers, with RBF, linear, sigmoid, and polynomial kernels, respectively.}
	\label{fig:confronto}
\end{figure}

As the graph shows, the four applied kernels perform similarly over considered features; the reason is that the boundaries between classes are almost linear. The average accuracy for all kernels is approximately 95.5 +/- 3.8\%. The LDA is placed after SVM with an average performance of 94.5 +/- 4.9\%. It performs the same as the SVM, probably because of the existence of linear boundaries between classes. The last two items of the graph (i.e. MLP1 and MLP2) belong to the result of MLPs with one and two hidden layers, respectively. The layout of the MLP was also adjusted before utilization. Several layouts were tested and the one that yielded the best result was selected. As can be seen, the accuracy of the MLP with one hidden layer drops approximately 6\%, while the MLP with two hidden layers performs with similar accuracy to the SVM and LDA.
Therefore, it is clear that an SVM offers classification performances that match or exceed other classifiers and furthermore is computationally efficient . SVM has a high potential for application as a core for classification in myoelectric control systems, and appear to be capable of recognizing patterns that are more complex.\newline
Better performances of SVM are also clear in \cite{yang2009emg}. The authors compare EMG pattern recognition implementation leveraging both ANN and SVM and show that the the second achieve higher classification performances. Actually we conclude that ANN is not the best choice for time-variant signals. Moreover, it would lose the advantages given by the NMF algorithm which separates the two parts of the signal and gives a pre-processing to the signal. Applying ANN would have meant another pre-processing completely unnecessary.
For these reasons, we decided to implement an SVM algorithm for the Gesture Recognition.

\subsection{Support Vector Machine on FPGA}

The rationale behind the design of the SVM classifier is the exploitation of the parallel computational power offered by the FPGA heterogeneous resources and the high memory bandwidth of the FPGA internal memories in the most efficient way, in order to speed up the decision function $f(x)$. The computation of $f(x)$ involves matrix-vector operations, which are highly parallelizable. Therefore, the problem can be segmented into smaller ones and parallel units can be instantiated for the processing of each subproblem. \newline
Papadonikolakis et al.\cite{papadonikolakis2010novel} proposed a scalable FPGA architecture for the acceleration of SVM classification, which exploits the device heterogeneity and the dynamic range diversities among the dataset attributes. The Support Vectors are loaded into the internal FPGA memories and then the classification dataset is loaded on the RAMs of the FPGA board. The data points are streamed into the FPGA and fed to each classifier hypertile, which is the processing unit of the architecture. The hypertile outputs the results of prediction, which are then added in parallel. The proposed FPGA classifier was compared to a C implementation on a PC with a 3GHz Intel CORE 2 DUO and 2GB of RAM. The proposed FPGA classifier presents a speed-up factor of 2-3 orders of magnitude, compared to the CPU implementation, while outperforming other proposed FPGA and GPU approaches by more than 7 times.

Another valuable example of SVM hardware implementation is the one described by Song and Wang in their paper of 2014 \cite{song2014fpga}. For linear SVM design, they built 4 different two-class linear datasets (dataset A, B, C, D) in order to test the classifier. According to the cumulative results of each dataset, the test results given by the classification system have an accuracy of more than 99\% and time consumption met their requirements for the design. The results accuracy may be slightly different based on the amount of training data and the density of the support vectors. In Figure \ref{fig:tab2lin} the histogram compares time consumption results obtained on PC and on FPGA. We can notice that the value of the latter is lower of approximately 30\%-50\% compared to the former.

\begin{figure}[htbp]
	\centering
	\includegraphics[scale=0.25]{Pictures/TAB2LINEAR.png}
	\caption{Testing results of linear models. The FPGA performances are expressed as a percentage in comparison to PC implementation}
	\label{fig:tab2lin}
\end{figure}


For the non-linear SVM classification design, 4 different non-linear datasets are built to test the non-linear classifier. Also in this case cumulative results are satisfactory. The calculation error is around 0.041\%, which is better than the linear design. Also the time consumption is reduced in this case, compared to the former algorithm, by 60\%.
\begin{figure}[htbp]
	\centering
	\includegraphics[scale=0.3]{Pictures/TAB2NONLINEAR.png}
	\caption{Testing results of non-linear models. The FPGA performances are expressed as a percentage in comparison to PC implementation}
\end{figure}

In general, whatever the application, it appears that a SVM implementation on FPGA is possible and allows better results in terms of time and energy consumption than any other hardware device.



\section{Diabetic Retinopathy detection through Convolutional Neural Network}


Diabetic Retinopathy (DR) is an eye disease affecting blood vessels in retina. DR is the leading cause of vision loss among adults, so its early diagnosis is crucial in order to prevent the appearance of this kind of disability. Diabetes, and more specifically high sugar level in blood, activates a series of biochemical reactions damaging blood vessels, in particular those having a smaller diameter.
As a consequence, one of the symptoms belonging to the first stage (non proliferative stage) of DR is a deformation of the retinal capillary walls, which collapse forming small expansions, known as retinal microaneurysm.\newline
Over the past years many algorithms have been developed and implemented in order to perform retinal image segmentation and microaneurysm (MA) detection. They can be divided in two main groups: the first  one is based on a more classical image processing approach, the second leverages Deep Learning. Nonetheless many attempts have been made to improve MA identification, there is still a need to explore  new strategies, as these algorithms have proven to generate a high number of false positive detected lesions.
 \singlespacing
 In the following paragraphs the two methodologies will be discussed and compared to highlight their positive and negative aspects. In the last two sections we will discuss some possible hardware implementation of CNNs and give also a brief overview on ANNs, as they share the same computational issues.
 
 \paragraph{The classical approach}\mbox{}\\
 
 The classical approach is the most widely spread method to achieve microaneurysm detection as it has been used in several works starting from 1990s until more recent years. Therefore it represents a standard methodology whose results have to be taken into account when validating a new algorithm.
 \singlespacing
 The classical approach process is composed by three main steps:

\begin{enumerate}
 \item{Image pre-processing and candidate MA extraction}
 
 During this stage, the fundus camera photography undergoes a first elaboration in order to obtain an image suitable for MA candidate identification and consequently classification. The applied techniques can differ a lot one from the other. Some examples of possible operations are:
 
 \begin{itemize}
 \item green band extraction, which is particularly useful as MA can be more accurately seen on this colour channel\cite{shah2016automated}
 \item image filtering such as enhancement and smoothing to improve image characteristics \cite{wu2017automatic}
 \item blood vessels removal to eliminate non MA pixels\cite{shah2016automated}\cite{inoue2013automated}
 \end{itemize}
 
 After these operations candidate MA can be identified on the image and feature extraction is possible.
 
 \item{Feature extraction}
 
 Feature extraction step consists of candidate MA analysis with the aim of selecting their main characteristics. These parameters will then be used to classify whether a potential detected lesion is a real MA or not.The type of features to be extracted are manually choosen from researchers when elaborating a new algorithm. According to \cite{shah2016automated} some appropriate parameters can be color-based features, hessian matrix based features and curvelet-based features. These elements are then stored into an array in order to be classified during the next stage.
 
 \item{ Classification}
 
 The classification purpose can be achieved through different strategies, involving various kind of classifiers.Some of the most employed are: Rule-based classifier (RBS), Linear discriminant analysis (LDA), Artificial neural network (ANN)\cite{frame1998comparison}, KNN-classifier\cite{wu2017automatic}. Classification procedure gives as an output the binary classification of each image pixel in MA lesion or healthy pixel.
 
 \end{enumerate}
 

The algorithm discussed before are usually evaluated on the ROC database (Retinopathy Online Challenge) \cite{niemeijer2010retinopathy} using an FROC curve and plotting sensitivity versus false positive rate per image. From 1990s to nowadays the sensitivity parameter has benefit form a high improval, upgrading from 12 \% to 48 \%\cite{shah2016automated}. 
 

\paragraph{Deep learning approach: CNN application to Diabetic Retinopathy}\mbox{}\\

In the last three or four years researchers have leveraged Convolutional Neural Network in order to target Diabetic Retinopathy detection. In literature we have found two main approaches to this problem: DR severity classification and lesion segmentation.Their characteristics are described below. 


\subparagraph{Diabetic Retinopathy severity classification}\mbox{}\\

Following the severity classification method, images are divided in five classes on the basis of an international standard: no signs of DR, mild DR, moderate DR, severe DR and proliferative DR.
 
In \cite{gulshan2016development}  the GoogleNet CNN structure is deployed to execute classification achieving good results in terms of sensitivity, specificity and area under the curve (Se = 97.5\%, Sp= 93.4\%). The principal feature of this type of network is the inception module, in which more convolutions are performed in parallel branches and  then reunited through a concatenation. According to\cite{pratt2016convolutional} also VGG network can address DR classification, achieving good results when evaluated (Se = 95\%, Sp= 75\%). VGG network is characterized by reduced filter size in the convolution layer (3x3) and a very deep architecture, meaning that the number of kernel used in each layer is really high. Small kernel size ensures the number of parameters is kept low with less computational effort.

As the classification is performed on the whole picture, fundus camera images need to be resized in order to reduce computation time. Nonetheless image size can not be reduced drastically for the reason that retinal lesions are small and, in case of a remarkable decrease in resolution, microaneurysm might become no more recognizable. In order to avoid this problem the minimum picture size is kept to 512 x 512 pixels. Therefore the main issue concerning this type of approach is large image size and consequently the necessity to use a network architecture composed by a high number of layers. 


\subparagraph{Lesion Segmentation} \mbox{}\\


This methodology has been developed in order to identify retinal lesions with a pixel-wise approach. The algorithm consists in extracting a window around every image pixel and establishing, through a CNN, whether the considered window contains a lesion or not. The procedure output is a map in which every pixel is labelled with a number between 0 and 1 representing the probability that the considered pixel is a lesion. The network is trained using windows containing lesions or not on the basis of a priori annotated datasets.

 This technique is suitable to be applied to different kind of lesions, such as microaneurysms\cite{haloi2015improved}, hemorrages\cite{van2016fast} and exhudates. Our specific work is inspired by \cite{van2016fast}, where hemorrages are identified using an architecture similar to VGG net and 41 x 41 windows around each image pixel. The obtained results are very promising achieving an area under the receiver operating curve (ROC) of 0.894 and 0.972 on two different datasets.
 
 The positive aspect of these algorithms is that every processed picture is small in comparison to DR severity classification, therefore the number of layers composing the network is limited to a maximum of six or seven steps. Anyhow every pixel needs to be analysed and consequently this method requires high computational power. 

 
 \paragraph{Classical and Deep learning approach: a comparison} \mbox{}\\

 The table below shows the main positive and negative aspects regarding the two described methods. 
 
 %\begin{table} [H]
 %\centering
 %\begin{tabular}{ l | l }

 % \textbf {Classical approach} & \textbf{ CNN approach}  \\ \hline
  %$\ominus$ Long pre-processing  &  $\oplus$ Little pre-processing  \\ \hline
  %$\ominus$ Manual feature choice & $\oplus$ Feature selected by the network \\ \hline
  %$\oplus$ Less computational effort & $\ominus$ Computational heavy \\ \hline
  % & $\oplus$ High performance in image classification\\
%\end{tabular}
%\caption{comparison between Classical and Deep learning method}
% \label{tab:compare}
%\end{table}


\begin{table}[H]
\centering
\caption{Comparison between positive and negative aspects of Classical and Deep learning approach}
\label{tab:cnn_classical}

\begin{tabular}{c|c|c}
\toprule
&\multicolumn{1}{c|}{\textbf{Classical approach}} &\textbf{CNN approach}\\

\midrule
\textbf{Advantages} & Less computational effort  & Little pre-processing\\
 &  & Feature selected by CNN\\
 & & High performance in image classification\\
\midrule
\textbf{Disadvantages} & Long pre-processing & Computationally heavy\\
 & Manual feature selection & \\

%\textbf{Advantages} & Less computational effort &   Little pre-processing, Feature selected by CNN, High performance in image classification \\ \hline
%\textbf{Disadvantages} & Long pre-processing, Manual feature selection & Computationally heavy \\\hline
\bottomrule
  


\end{tabular}
\end{table} 




Concerning our work we have choosen to deploy CNNs as they are highly promising in targeting biomedical image classification\cite{greenspan2016guest}. Despite this strong point, the algorithms are computationally heavy when executed on a CPU (off-the-shelf Hardware) and need to be accelerated. The previously discussed methodologies have been all implemented on GPU cards, since they are specifically designed to deal with arrays and matrices. As far as we are aware nobody has never  leveraged  FPGAs for DR recognition through CNNs, even if  it might be an interesting choice in order to reach a satisfactory trade-off between power consumption and algorithm speed-up; 
 Between the two CNN based approaches (DR severity classification and Lesion segmentation) we have selected the second methodology as it directly deals with our specific problem: microaneurysm detection.





\subsection{Artificial Neural Network on FPGA}

The processing element of an ANN is the neuron. The structure of a neuron is split into various sub blocks. These blocks are implemented individually, first, and then integrated to form the entire neuron. The major issues in the realization of the computational blocks using FPGAs are: parallel/sequential implementation, bit precision and use of LUT for nonlinear function\cite{muthuramalingam2008neural}. To reduce costs related to parallel execution, the operations are carried out sequentially, which in turn reduces the speed of computation. Selecting bit precision is another important choice when implementing ANN on FPGA. A higher bit precision means fewer quantization errors in the final implementations, while a lower precision leads to simpler designs, greater speed and reductions in area requirements and power consumption. LUT improves speed of operation, but higher precision demands larger memory. \\
In order to implement bigger ANN on FPGA it is possible to use several FPGA on each layer, as shown in figure \ref{fig:annfpga}.
\begin{figure}[htbp]
	\centering
	\includegraphics[scale=0.5]{Pictures/FPGAANN.png}
	\caption{ANN composed by more neurons \cite{raeisi2006implementation}}.
	\label{fig:annfpga}
\end{figure}

The neuron can be viewed as processing data in three steps; the weighting of its input values, the summation of them all and their filtering by sigmoid function. The sigmoid function is a common activation function: $y = \frac{1}{1+\varepsilon^{-x}}$.
\newline
Efficient implementation of the sigmoid function on an FPGA is a difficult challenge faced by designers, for the reason that it is not suitable for direct implementation because of its infinite exponential series.\newline
In \cite{elrharras2015fpga} another FPGA-based implementation of ANN is reported. The decision is performed by a multi-layer neural network with two layers in which every single layer is completely linked to its closest. %Its implementation is done on an FPGA device, as it offers a high flexibility.
 %VHSIC Hardware Description Language (VHDL) is used to synthesize the design, with Altera Quartus II Web Edition as the place and-route tool. The implemented network architecture consists in using 10 neurons at the input layer. 
VHDL Hardware Description Language is used to synthesize the design, with Altera Quartus II Web Edition as place and-route tool. The implemented network architecture is composed by 10 neurons in the input layer. 
Thereafter, 10 neurons are inserted in the hidden layer, while one neuron is exploited in the output layer to differentiate between the two hypotheses. The obtained accuracy is high, around 98\%, and resource usage is efficient.\\
 A deep analysis of ANN on FPGA is also developed in \cite{sartin2014ann}. This work proposes the implementation of a system in floating point for the approximation of a function using non linear ANN. As a result, it is proven that the use of FPGA for ANN is suitable for the level of parallelism and the  execution time in hardware is 275 times faster than software.\newline
As a consequence, it appears  clear that also this algorithm guarantees high performances if implemented on FPGA but for our two case studies it was not suitable for the reasons we explained in Sections \ref{sec:cnn} and \ref{sec:gesture}. %Especially in images classification it fails in accuracy and therefore it isn't usually implemented for this application. For this reason, and for the ones mentioned in 3.1, we didn't implement this algorithm. 
% In Yousef (2012) \cite{youssef2012reconfigurable} a simple 2-4-1 Neural-Network is implemented and trained in MATLAB, then the weights values are loaded into a design GUI and converted to binary representation. After that a functional simulation is performed on Xilinx ISE design suite 12.1, the outputs of the Neural-Network are saved on text file and finally loaded into MATLAB. The network is implemented in two design solutions to test the possibility of fitting bigger applications in small area. The first solution is based on 4 neurons, the second with only one. This is a simple example proving how the design can be reconfigured for different speed and area specification. A comparison is made between the speed of the two design settings, the speed of software (MATLAB) and also the speed of general purpose DSP(digital signal processors). The comparison shows an impressive speed-up of this design in respect to DSP. It was also estimated an overhead in speed when using 1 neuron instead of 4 neurons. The most notable result is that this solution is 60 times faster than the DSP solution.

\subsection{Convolutional Neural Network on FPGA} 
\label{sec:cnn_on_fpga}

Considering CNN high parallelizability, in the past years many different FPGA architectures have been deployed in order to accelerate the algorithm execution time. Each work targets a particular issue concerning CNN implementation on FPGA.\\ Farabet  \cite{farabet2009cnp} focuses its attention on the convolutional layer and memory architecture. The considered system (Figure \ref{fig:farabet}) is composed by a control unit (CU) communicating with the PC, a Vectorial ALU (VALU) and a dedicated memory structure. The highly parallel and pipelined VALU is in charge of performing all the computations required for a CNN, such as the convolution, the sub-sampling and the sigmoid activation function. In particular for the convolution step all the Multiply and Accumulate operations (MAC) are performed in a parallel manner, so that the execution number of clocks is equal to the number of points composing the output feature map. The memory management controller leverages direct memory access in order to speed-up data transfer. The circuit is implemented on two different devices: a Xilinx Spartan 3 coupled with Xilinx development kit and a Virtex 4 with a custom design. In the former case the achieved result is 6 processed frames (512 x 384) per second, while in the latter 10 frames are obtained per second; this high difference in time performance is connected to the bandwidth limitation of the external bus: 1 GB/s for the first solution and 7.2 GB/s for the second. Indeed the external memory bandwidth is one of the main issue concerning FPGA implementations.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.8]{Pictures/farabet.PNG}
	\caption{FPGA-based system with VALU and direct memory access \cite{farabet2009cnp} }
	\label{fig:farabet}
\end{figure}
An interesting implementation is presented in \cite{qiu2016going} where the main improvement consists in data and weight quantization. In particular a VGG16 network is implemented on FPGA with two different configurations: 32 bit floating-point precision and 8 bit fixed-point precision. Despite data and weight precision reduction, the accuracy is kept high varying from 88 \% to 87.6\% (-0.4\%). On the other hand the number of resources involved in terms of LUT, FF and memory bandwidth decreases considerably.\\
Another very recent FPGA implementation \cite{FPGA_Bacis} proposes  a dataflow computation pattern in which a pipelined and scalable architecture is obtained. This work leverages the SST (Stream Stencil Timestep) system in order to ensure that input data is read only once from on-chip memory and is streamed throughout the network architecture. An SST (Figure \ref{fig:sst}) is composed by two elements: a memory unit and a computing unit. The first one is composed by a series of chain of filters interconnected by FIFO channels. Each filter reads any existing data element from its preceding FIFO, sending it always to the subsequent one in the chain, if any, and, when the correct data element arrives, it sends it to the computing system to allow it to produce output. The computing system contains all the CNN layers: convolution, sub-sampling and fully connected. This approach was tested on a Xilinx Virtex 7 where a CIFAR-10 network was implemented. The results outperform previous works by a factor of 3.36x achieving 7809 image processed per second versus the previous 2318.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.8]{Pictures/sst.PNG}
	\caption{An SST system with separation between memory and computing unit\cite{FPGA_Bacis} }
	\label{fig:sst}
\end{figure}

Finally, other recent and interesting works \cite{del2016automation} \cite{zhang2016caffeine} take into account the possibility of creating FPGA-based frameworks for convolutional neural networks, giving to the final user a tool to build its own prediction network on FPGA with a simple web-based application. As an example in \cite{del2016automation} the application generates a synthesizable C++ code that is transmitted to Vivado HLS and then employed to generate the bitstream file using Vivado Design Suite. Then the FPGA (in this case a Zedboard or a Zybo) is configured by means of the bitstream. This type of user-friendly applications can be considered the future of CNN design on FPGA, as they could fill the gap between CNN hardware design and final user.



%In Sankaradas (2009) (?? cite) [24] a massively parallel, programmable coprocessor for CNNs is architected. Their aim is to demonstrate that significant performance improvements can be achieved at low power budgets. Even high-end FPGA prototype dissipates less than 11W of power. While the target fort the solution is an application-specific standard product (ASSP) for embedded domains with real-time constraints, servers where power is critical can also avail of our solution, perhaps even as an FPGA add-on card. They designed a prototype of the coprocessor using an off-the-shelf FPGA board from Alpha Data. The board has a Xilinx Virtex-5 LX330T device. To use this prototype, a CNN is decomposed into primitive convolutions and data movement instruction (Section III.B.3)). The resulting convolutions are statically scheduled and mapped to the hardware. The adder stage computes sums of individual convolution results together with a programmable bias. The non-linear function, specified by the host via the API, is then applied to the summed results. A specialized sub-sampler hardware unit is available if required by the specific CNN being processed. Sub- sampling weights are also specified by the host, and programmed into the sub-sampling hardware. Here we report the raw speed-up achieved by the FPGA-based CNN processor. A CNN for a face recognition application is used. The application examines every 64x64 pixel ?window? in each frame of VGA resolution moving images, and detects up to 1 full, unobscured face per window. 
%\begin{figure}[htbp]
	%\centering
	%\includegraphics[scale=0.3]{Pictures/CNN_results.png}
	%\caption{Speed-up for the prototype for the CNN of Sankaradas}
	%\label{fig:cnnresults}
%\end{figure}

%Fgure \ref{fig:cnnresults} compares the FPGA speed a non-optimized software implementation of the CNN, and with an SSE2 optimized case where special SIMD instructions are used to perform many of the computations in the CNN.  The results show 31x speed-up for the FPGA over the base software implementation, and nearly a 4x speed-up over SSE2 optimized software.

