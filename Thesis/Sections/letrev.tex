\chapter{Literature Review and Problem definition}


First of all we deepened our knowledge about the main characteristics of the chosen algorithms. This was necessary in order to understand their main advantages, disadvantages and  possible applications. As a first approach we focused on three algorithms because we could not know in advance which were the best in terms of accuracy and suitability to our case studies. In this section an explanation of the algorithms is provided as well as the main issues affecting this type of algorithms and our proposed solution. %A brief overview on the State of Art of our two case studies will also be discussed.

\section{The Algorithms}
\label{sec:algorithms}

The algorithms we decided to analyse are: Support Vector Machines, Artificial Neural Networks and Convolutional Neural Networks. There are many different valid algorithms and each of them is more convenient for certain applications than others. Actually, our main objective was to find the best possible accuracy, and to this aim, we extensively investigated their characteristics.
%we had to choose the correct algorithm for each case study. 
%The analysis of the State of Art helped us in this decision, reducing the number of algorithms from three to two. In the following paragraphs we will also give reasons why one of the previously mentioned algorithm was not suitable for our purpose.

\subsection{Support Vector Machine}


The Support Vector Machines are a powerful supervised learning method, which provides State of Art accuracy in various classification
tasks. SVM is a binary classifier, with the possibility of classifying samples in multiple classes. Moreover it can be used
to solve either linear and non linear classification or regression
problems. SVM makes use of the concept of structural risk minimization which provides a trade-off between the empirical error and the model complexity. The empirical error is defined as follows:
\begin{equation}
R_{emp}(\alpha )=\frac{1}{2l}\sum_{i=1}^{l}\left | y^{i}-f(x^{i},\alpha ) \right |
\end{equation}
where $l$ is the number of observation, $y^{i}$ represents the label, $\alpha$ represents the vector of editable parameters, and $f$ represents the prediction function.    %learning machine.
In a classification problem, the objective of the SVM training task is the construction of a hyperplane which will provide maximum separation between the classes (Figure \ref{fig:hypsvm}). The outcome of the training phase is a set of support vectors, i.e. critical points of the different classes that define the classification hyperplanes.
\begin{figure}
	\centering
	\includegraphics[scale=0.5]{Pictures/hyp.png}
	\caption{Plane of separation between two classes in a three-dimensional space.}
	\label{fig:hypsvm}
\end{figure}

%The SVM training builds a model that is able to predict the class of any future data according to its distance from the support vectors obtained by the training dataset.\newline

Before starting the training phase, the input data must be set in a hyperplane, a plane of N dimensions, where N is the number of features of each observations. These features are the characteristics the algorithm will use to discriminate the observations and associate them to a class instead of another. Thus, the SVM training builds a model that is able to distinguish the belonging class based on the support vectors obtained by the training dataset. On the prediction phase, any new sample $x$, i.e. a new vector of data, is classified according to the output of the decision function. The decision function is built using the coordinates of the optimal estimated hyperplane and its bias, that represents the offset. This function will differ according to the type of kernel used. Kernels are used to map the input data to the feature space and they are commonly of four types: linear, polynomial, radial basis function (RBF) and sigmoid \cite{hsu2003practical}.

A linear kernel can be used for linearly separable data, i.e. data that can be separated with an hypersurface defined by a polynomial of grade one. \cite{mahmoodi2011fpga}.

In this case, the SVM discriminant function has the form:
\begin{equation}
f(x)=W^T \textbf{x}+b
\end{equation}

where $W$ represents the weight vector, $b$ is the bias or offset scalar and \textbf{x} represents the input vector. The classification rule is $sign(f(x))$, and
the linear decision boundary is specified by $f(x)=0$. The labels  are decided on the basis of the sign of the decision function $y\in \{ -1,1\}$.\newline

More difficulties arise when data are non-separable.
In this case the optimization problem includes a C parameter, which is user-defined. This is a penalty parameter of the error, which controls the balance between the complexity of the SVM and the number of non-separable points. 
With its value, the user is able to control the SVM ability of generalizing. This problem is then solved using a quadratic optimization.\newline
The classification task becomes very time consuming for large-scale problems, because SVM depends on the data load and the population of Support Vectors.
Yet SVM classification can be accelerated, especially for applications which need to perform in real-time on newly obtained data, as we will explain in section $3.1.1$. 

\paragraph{SVM multi-class}\mbox{}\\

The SVM, inherently, is a binary classifier, while many problems we are interested in solving (such as the classification of limb motions), are multi-class. As SVM performs very well for binary problems, it is desirable to extend its capabilities to multi-class problems.
In a multi-class classification problem, the task consists in selecting one label from $N > 2$ possible choices. Error Correcting Output Codes (ECOC) is a method which combines many binary classifiers in order to solve the multi-class problem \cite{dietterich1995solving}.
Both one-versus-rest and one-versus-one are special cases of the ECOC, which decomposes the multi-class problem into a predefined set of binary problems. We decided to focus on the one-versus-one method. This methodology divides the original problem of $N$ classes in $n$ binary problems, so that the output structure comprises $n = \frac{(N)(N-1)}{2}$ binary learners and one classifier for each pair of labels.

%There will be needed as many decision functions as the binary learners and the belonging to a class will be based on a coding matrix $M_{Nxn} \in \{ -1,1\}$. It is a matrix that represents for each row, thus for each class, the outputs the decision fuctions should have to codify for that particular class.

Then a coding matrix is generated  $M_{Nxn} \in \{ -1,1\}$.
Each of the N classes is assigned to a unique binary string, known as codeword, which represents the rows of M. On the other hand the columns of M denote the classifiers. Once M is defined, the $n$ classifiers consequently learn the $n$ binary problems coded in M. The class that gains more votes by the binary classifiers is then chosen as the final output.


\subsection{Artificial Neural Network}

The Artificial Neural Networks (ANN) are inspired by biological neural systems. The transmission of signals in biological neurons through synapses is a complex chemical process in which specific transmitter substances are released from the sending side of the synapse. The effect is to raise or lower the electrical potential inside the body of the receiving cell. If its potential reaches a certain threshold, the neuron fires \cite{ali2010design}. It is this characteristic of the biological neurons that the artificial neuron model proposed by McCulloch Pitts attempts to reproduce (Figure \ref{fig:layerann}).\newline
The neuron model is widely spread in Artificial Neural Networks with some variations. The artificial neuron represented in Figure \ref{fig:layerann}  is characterized by $ n $ inputs, denoted as $ x_{1} $,...$ x_{n} $. A weight, denoted as $ w_{1} $,$ w_{2} $,...$ w_{n} $ respectively, is assigned to each line connecting these inputs to the neuron. The activation, $ f $, determining whether the neuron has to fire or not, is given by the formula: $ f=\sum_{i=1}^N w_i x_i $. The bias, $ b $, varies according to the propensity of the neuron to be activated, thus changing its activation threshold. A negative weight indicates an inhibitory connection while a positive value corresponds to an excitatory connection. \\

A neuro-computing system is made of a very high number of artificial neurons and a huge number of interconnections between them. In layered neural networks, the neurons are organized in the form of layers.  The neurons in a certain layer get inputs from the previous one and feed their outputs to the next layer. These types of networks are called feed-forward networks. Output connections from a neuron to the same or previous layers are not permitted. The input layer is made of special input neurons, transmitting the applied external input to their outputs. The last layer is called the output layer, and the layers other than input and output are called the hidden layers. A network including only input and output layers is called single layer network. Networks with one or more hidden layers are named multilayer networks. \newline

\begin{figure}
	\centering
	\includegraphics[scale=0.5]{Pictures/layers.jpg}
	\caption{Structure of ANN and division in layers \cite{babu2014moving}.}
	\label{fig:layerann}
\end{figure}


The backpropagation algorithm is a type of supervised learning, used to train neural networks and reduce the total error of the system. Once the neural network produces an output, the error between the real output and the desired output is calculated. This error is backpropagated causing the change of weights. The process is repeated until the error is minimized enough to gain acceptable results. Since the back propagation algorithm uses an instantaneous estimation of the error, it follows a random path and eventually converges to a minimum error. However, this minimum error is typically a local minimum and there is no guarantee that is also a global minimum. \newline
%The activation function is usually an abstraction representing the rate of action potential firing in the cell. In its simplest form this function is binary, that represents either the neuron is firing or not. In other words, it represents how the input values are combined to determine the overall signal output.\newline

Therefore the ANN algorithm can be summarised as follows:
\begin{enumerate}
\item Load values of inputs $x_{i}$ and weights $w_{i}$;
\item Estimate the sum of input values weighted with their $w_{i}$;
\item Estimate the value of the activation function $f$ using the result of the weighted sum;
\item The output $y$ is the result of the activation function:
\begin{equation}
 y(x) = f(\sum_{i=1}^N w_{i}x_{i}+ w_{0})=\bar{w}^{T}\bar{x}
\end{equation}
\end{enumerate}

Concerning advantages and disadvantages, Artificial Neural Networks are remarkable classifiers. One of their main benefits is that they have a great ability to generalize, as they can produce reasonable results even based on never seen inputs.
Although the training procedure can generate acceptable results, the process may take a very long time because of the stochastic nature of the learning algorithm. Moreover the algorithm is not able to explain the mathematical phenomenon beside the decision of outputs.

\subsection{Convolutional Neural Network}
\label{sec:cnn}

The third algorithm, Convolutional Neural Networks (CNN), belongs to Deep Learning field and consists of a cascade of many processing layers aimed at performing feature extraction and data classification; it is particularly suitable for image classification and computer vision. Indeed an important remark is that among the three considered classifiers (Section \ref{sec:algorithms}) CNNs are the only one which are well suited for images. The reason is that SVMs and ANNs have proven to generate unsatisfactory results when applied to images, as they do not exploit data bidimensionality. On the other hand CNNs are based on convolution, which is able to extract proper features from images. \singlespacing
  Convolutional Neural Networks are similar to Artificial Neural Networks, as both of them are composed by layers of neurons connected through a set of weights and biases\cite{cnnstanford}. As in ANNs, each neuron receives inputs from the previous layer and performs a weighted sum of  dot products. Differently from ANNs, in the case of CNNs neurons are arranged in volumes; moreover there are different types of layer building the network, each operating a particular function. A single layer (except the Fully Connected Layer) receives a 3D volume as an input and transforms it into a new output 3D volume. An example of a CNN structure is represented in Figure \ref{fig:cnn} and its basic components are described afterwards. All layers are customizable, i.e. the parameters describing a single layer can be fine-tuned and can consequently improve or worsen the network performances. The following description is only an overview on the first applied CNN, LeNet5\cite{lecun1998gradient}; indeed in the last years many new CNN structures have been implemented in order to increase performances in image classification.
 
 %Correct image positioning
 \begin{figure} [H]
 \centering
 \caption{CNN structure example with its main layers: Convolution, Subsampling and Fully Connected (LeNet5 \cite{lecun1998gradient})}
 \label{fig:cnn}
 \includegraphics[scale=0.8]{Pictures/LeNet}
 \end{figure}
  
  
 \begin{enumerate}
 
 \item{Convolutional Layer}
 
 This layer receives as an input a neuron volume and performs a 3D convolution with small learnable filters extending over the whole depth of the input volume; the applied operation is expressed through the following equation, where $ w $ are weights and $ b $ are biases:
 
\begin{equation}
 o_{k,i,j} = \sum_{m=0}^M\sum_{n=0}^N (w_{k,m,n} \cdot x_{i+m,j+n})+b_k
 \label{ref:convolve}
\end{equation}

  Each filtering passage produces a single feature map and collecting all the feature maps we obtain the 3D output volume. This process is graphically represented in Figure \ref{fig:conv}.
 The convolutional layer plays a fundamental role in CNNs as it performs feature extraction and it reduces the height and width dimensions of the input volume. \singlespacing
 Moreover, it can be followed by an activation function such as Sigmoid, Tanh, ReLU and Leaky ReLU. Anyhow the most common inserted in CNNs is the Rectified Linear Unit (ReLU) defined as follows:
 
\begin{equation}
f(x) = max(0,x)
\label{ref:relu}
\end{equation}

The customizable parameters belonging to this layer are: height and width of the kernel, the number of kernels applied to perform convolution and stride. 
  

 \begin{figure}[H]
\includegraphics[scale=0.25]{Pictures/convolution}
\centering
\caption{Convolutional layer structure: each kernel is convolved with the input volume, generating a feature map. Collecting all the feature maps the output volume is obtained.}
\label{fig:conv}
\end{figure}

 
\item{Pooling Layer}

The pooling layer is a subsampling layer whose role is to reduce the spatial size (height and width) of the input volume without affecting depth. Different types of pooling have been used in the State of Art, for instance Max-pooling, Average-pooling, Median-pooling. The most widely spread is Max-pooling, computing the maximum over a NxM window, where N and M are window parameters defined when building the network architecture. In Figure \ref{fig:pool} the sub-sampling operation is graphically represented. 

 \begin{figure}[H]
\includegraphics[scale=0.3]{Pictures/pool}
\centering
\caption{Pooling layer: a NxM window (here N=2, M=2) is shifted over the image and the chosen operation is performed (average, maximum, median). The result is stored in the output matrix.}
\label{fig:pool}
\end{figure}

 
 \item{Fully Connected Layer}
 
 The Fully Connected Layer is a linear layer similar to those composing a regular Artificial Neural Network. It receives a 3D input volume, unrolls the volume and performs a weighted sum of  products between all the neurons belonging to the input volume and a set of weights. The Fully Connected structure can be composed by multiple hidden layers. The output elements of these layers are calculated by the following equation:
 
 \begin{equation}
o_j = \sum_{i=0}^I (w_i \cdot x_i) + b_j
\label{ref:fc}
\end{equation}

The last layer of the linear part of the network contains as many neurons as the classes to be recognized. Usually, it is followed by the LogSoftMax operator, which normalizes results in the following way:
 
  \begin{equation}
p(\textbf{z})_j = \frac{e^z_j}{\sum_{k=1}^K e^z_k}\qquad  for   j=1, ..., K
\label{ref:softmax}
\end{equation}

LogSoftMax enforces the K values of the output vector z to lie in range [0,1] and to sum up to 1, such that they can be interpreted as the probability of the input to belong to a certain class (i.e. the maximum probability).
   
 \end{enumerate}
 

\section{The role of the FPGA}
  
\subsection{Problem definition} 

The main issue to be aware when dealing with classification algorithms is their computational heaviness, for the reason that they include a high number of vector, matrix and tensor computations involving floating point operators. Therefore through the years this characteristic has become a concern for designers, in particular for those systems which involves as-a-service applications or real-time signal analysis and, consequently, where high performances are required. In particular in the Biomedical field real-time analysis is an important constraint as it plays a major role in diagnosis and patient care. \newline

On the other hand classification algorithms possess some highly positive characteristics which can be deployed in order to meet these really strict requirements.
Indeed parallelism, modularity and dynamic response are three computational characteristics typically associated with classification algorithms. Every algorithm is composed of several steps, which involve different operations to be performed continuously. Most of these operations are independent to each another, so that they can be parallelized. This means that the execution of these processes can be carried out simultaneously. In particular, exploiting task parallelism, entirely different calculations can be performed on either the same or different sets of data. Task parallelism involves the decomposition of a task into sub-tasks and then allocating each sub-task to a core for execution. The different cores would then execute these sub-tasks simultaneously and often cooperatively, leading in this way to an important time speed-up \cite{computing2013journal}.\newline

However in order to reach such a parallelization, particular hardware architectures must be taken into account, as common CPU processors are not intrinsically parallel and thus do not guarantee high and real-time performances with classification algorithms. In the subsequent section the most important existing architectures will be analysed and compared to highlight their main characteristics and to explain why we consider FPGAs the most suitable choice for our case studies.



%Parallelism, modularity and dynamic response are three computational characteristics typically associated with classification algorithms. Every algorithm is composed of several steps, which involve different operations to perform continuously. Most of these operations are independent to each another, so that they can be parallelized. This means that the execution of these processes can be carried out simultaneously. In particular, exploiting task parallelism, entirely different calculations can be performed on either the same or different sets of data. Task parallelism involves the decomposition of a task into sub-tasks and then allocating each sub-task to a core for execution. The processors would then execute these sub-tasks simultaneously and often cooperatively. \ref {computing2013journal}
%\\However, parallel computations require larger resource and they are therefore costly in terms of time and power consuming. Morever, the complexity of the Classification Algorithm itself makes the process computationally very intense. %[citare testo]
%Through the years these features have become a concern for designers, in particular for those programs which involves as-a-service applications or real-time signal analysis and, therefore, where high performance are required..

\subsection{The solution: Reconfigurable computing}

Among many solutions exploited to support classification algorithms, we have analysed the most common devices in the State of the Art: Off-the-Shelf devices, Full-Custom hardware and Reconfigurable devices, which are compared in Figure \ref{fig:hw} .\\

Off-the-Shelf devices are easily programmable, flexible and they perfectly support any change in the implementation. Their main characteristic is that their architecture and data precision is fixed a-priori when designing the device in industry and can not be changed by the final user. 
Different types of Off-the-Shelf devices are available on the market among which the most exploited for classification algorithms are CPUs and GPUs (Graphics Processing Unit).\\ Concerning CPUs, they show low performances dealing with time-execution of parallel operations and coarse-grained algorithms, as they execute instructions in a sequential manner.  However compared to the other solutions, they are definitely more affordable in terms of purchasing cost.  In the medical device industry, Off-the-Shelf software can sometimes be identified as software that has not been developed with a known software development process or methodology, which precludes its use in medical devices. In this industry, faults in software components may become system failures in the device itself. \\On the contrary, GPUs are specialized circuits, originally designed to manipulate images and are consequently highly deployed by engineers, as they offer good parallelization and capabilities of performing computations between matrices, vectors and tensors. Indeed they are the State of Art for Convolutional Neural Network implementation. However our specific work does not target GPUs as their performance in terms of power consumption is low in comparison to the other solutions we will discuss afterwards.\\

Full Custom devices (such as ASICs) offer the best results in terms of time and power performances, but they are rather inflexible in term of code adaptability and reusability. This is due to the fact that the layout of each individual transistor and the interconnections between them are designed and deployed for a specific, fixed task and can not be changed after the final implementation. As a consequence, a change in the algorithm would involve a change in the mask and the necessity to produce the circuit again. Thus, Full-custom design potentially maximizes the performance of the chip, and minimizes its area, but is extremely labor-intensive and expensive to implement.\\

The compromise is embodied by Reconfigurable Computing and FPGAs.\\ \itshape\begin {center}``Reconfigurable computing is intended to fill the gap between hardware and software, achieving potentially much higher performance than software, while maintaining a higher level of flexibility than hardware.''\end{center}
\upshape\begin{flushright}K. Compton and S. Hauck,\\ \textit{``Reconfigurable Computing: a Survey of Systems and Software''}, 2002\end{flushright} 
The advantages of the FPGA are the ability to exploit fine and coarse-grained parallelism, the capacity to adapt its architecture to different algorithm requirements keeping the adaptability and flexibility of software development and, finally, embodying the performance of hardware with a great performance/power ratio. FPGA-based hybrid systems have shown several positive characteristics that make them very attractive for high performance computing (HPC). The impressive speed-up factor that they are able to achieve, the reduced power consumption, and the easiness and flexibility of the design process with fast iterations between consecutive versions perfectly fit the issues, listed above, related to the Classification Algorithms.

\begin{figure}[htbp]
	\centering
	\includegraphics[scale=0.6]{Pictures/FPGAgraph.png}
	\caption{Comparison between different type of hardware devices. Full Custom devices maximize performances, yet they are not flexible. Off-the-Shelf hardware is the most flexible solution, but its performances are low. The trade-off between them is represented by Reconfigurable Computing}
	\label{fig:hw}
\end{figure}


Finally, other possible types of more sophisticated architectures have been recently implemented in order to directly target classification and Machine Learning algorithms. In particular it is worth citing the IBM Truenorth and the TPUs (Tensor Processing Units). IBM Truenorth is a VLSI chip containing electronic analogue circuits which are able to mimic neuro-biological networks. It contains 4096 cores, each comprising 256 neurons. It is energy efficient and can be leveraged in order to implement Artificial Neural Networks. TPUs are ASIC circuits developed by Google researchers and specifically designed to accelerate training and classification for Machine Learning algorithms. They are characterized by low bit precision and integer and floating point operations.

%Among many solutions exploited to support the classification algorithm, we have selected the most common devices in the State of the Art: Full-Custom hardware, Off-the-Shelf devices and Reconfigurable devices, which are compared in Figure \ref{fig:hw} .\\Full Custom offers good results in terms of performance, but they are rather inflexible in term of code adaptability and reusability. This is due to the fact that the layout of each individual transistor and the interconnections between them are designed and deployed for a specific, fixed task. As a consequence, a change in the algorithm would involve a change in the mask.Thus, Full-custom design potentially maximizes the performance of the chip, and minimizes its area, but is extremely labor-intensive and expensive to implement.
%\\On the other hand, Off-the-Shelf devices are easily programmable and they perfectly support any change in the implementation. Compared to Full Custom devices, they are definitely more affordable in term of purchasing cost. However, they show low performances dealing with time-execution of parallel operations and coarse-grained algorithm. In the medical device industry, Off-the-Shelf software can sometimes be identified as software that has not been developed with a known software development process or methodology, which precludes its use in medical devices. In this industry, faults in software components may become system failures in the device itself.\\The compromise is embodied by Reconfigurable Computing and FPGA.\\ \itshape\begin {center}``Reconfigurable computing is intended to fill the gap between hardware and software, achieving potentially much higher performance than software, while maintaining a higher level of flexibility than hardware.''\end{center}
%\upshape\begin{flushright}K. Compton and S. Hauck,\\ \textit{``Reconfigurable Computing: a Survey of Systems and Software''}, 2002\end{flushright} 
%The advantages of the FPGA are the ability to exploit fine and coarse-grained parallelism, the capacity to adapt its architecture to different algorithm requirements keeping the adaptability and flexibility of software development and, finally, embodying the performance of hardware with a great performance/power ratio. FPGA-based hybrid systems have shown several characteristics that make them very attractive for high performance computing (HPC). The impressive speed-up factors that they are able to achieve, the reduced power consumption, and the easiness and flexibility of the design process with fast iterations between consecutive versions perfectly fit the issues, listed above, related to the Classification Algorithms.

%\begin{figure}[htbp]
	%\centering
	%\includegraphics[scale=0.6]{Pictures/FPGAgraph.png}
	%\caption{Reconfigurable Computing}
	%%\end{figure}

\subsection{FPGA structure}

%In 1985, Xilinx company introduced a completely new idea in digital integrated circuit (IC) design. The concept was to combine the user control and time market of PDLs  with the compactness and cost benefits of gate array. The idea was brilliant and the FPGA was born. Today Xilinx is still the number one FPGA vendor in the world, followed by Altera.
A field-programmable gate array (FPGA) is a logic device that contains a two-dimensional array of configurable logic blocks (CLBs) and programmable switches. Using CAD software programming tools, a user can implement its own designs on the FPGA  \cite{harris2010digital}.
The structure of an FPGA device is shown in figure \ref{fig:fpgaarch}.

\begin{figure}[htbp]
	\centering
	\includegraphics[scale=0.7]{Pictures/fpga_chip_diagram.png}
	\caption{ The following illustration shows the relationship between logic blocks, I/O blocks and programmable routing or programmable switches on an FPGA.
\cite{fpgaandillustrator}.}
	\label{fig:fpgaarch}
\end{figure}

Each CLB can be configured (i.e. programmed) to perform combinational or sequential functions. The CLBs are surrounded by input/output blocks (IOBs) for interfacing with external devices. CLBs can connect to other CLBs and IOBs through programmable switches that can be customized. 
The CLB consists of a pair of slices, depending on the FPGA model. Those slices are divided into two logic elements that contain: 
\begin{itemize} 
\item lookup tables (LUTs) that implement the combinational logic functions 
\item configurable multiplexers used for logic selection
\item registers (D flip flop) that store the output of LUTs.
\end{itemize}
Furthermore, modern FPGAs contain additional components, such as random access memory blocks (BRAMs) as local memory and digital signal processor blocks (DSP blocks) as efficient computation units.\newline
In summary, all these elements provide the advantage of  re-programmable hardware architecture.
The design configuration of the FPGA architecture is generally specified using a HDL (Hardware Description language), a computer language used to describe the structure and the behaviour of electronic circuits. The design is then synthesized into the FPGA. The synthesis tool determines how the LUTs, multiplexers, and routing channels should be configured to perform the specified functions. This configuration information is then downloaded to the FPGA.
%Every FPGA chip is composed of a finite number of configurable logic blocks(CLBs) connected via programmable interconnects, as said before. The configuration of the CLBs and interconnections implement the logic designed inside the device. Nowadays, a CLB usually consists of multiple look-up tables (LUT),flip-flops and MUX.  Look-up tables are how your logic actually gets implemented. In particular, a LUT stores a predefined list of outputs for every combination of inputs and what makes a LUT powerful is that you can program what the output should be for every single possible input. Each LUT's output can be optionally connected to a flip-flop. Furthermore, modern FPGAs contain additional components, such as random access memory blocks (BRAMs) as local memory and digital signal processor blocks (DSP blocks) as efficient computation units. All this elements work together to create a very flexible device. \newline

%Field Programmable Gate Arrays (FPGAs) are semiconductor devices and consists of an array of uncommitted elements that can be interconnected in a general way and are user-programmable \cite{brown2012field}. It consists of a two-dimensional array of logic blocks that can be connected by general interconnection resources. The interconnect comprises segments of wire, where the segments may be of various lengths. Present in the interconnect are programmable switches that serve to connect the logic blocks to the wire segments or one wire segment to another. Logic circuits are implemented in the FPGA by partitioning the logic into individual logic blocks and then interconnecting the blocks as required via the switches. To facilitate the implementation of a wide variety of circuits it is important that an FPGA is as versatile as possible. This means that the design of the logic blocks, coupled with that of the interconnection resources, should facilitate the implementation of a large number of digital logic circuits. There are many ways to design an FPGA, involving trade-offs in the complexity and flexibility of both the logic blocks and the interconnection resources. The structure and the content of a logic block are called its architecture and can be designed in many different ways. There exists a myriad of possibilities for defining the logic block as a more complex circuit, consisting of several sub-circuits and having more than one output. Most logic blocks also contain some type of flip-flops, to aid in the implementation of sequential circuits.\newline
%A single FPGA can replace thousands of discrete components by incorporating millions of logic gates in a single integrated circuit (IC) chip. In figure \ref{fig:FPGA} it is shown the scheme of an FPGA.
%\begin{figure}[htbp]
	%\centering
	%\includegraphics{Pictures/fpga.jpg}
	%\caption{FPGA allows the user to program gates into parallel hardware paths \cite{imgfpga}.The following illustration shows the relationship between logic blocks, I/O blocks, and programmable interconnections on an FPGA.}
	%\label{fig:FPGA}
%\end{figure}

\subsection{High-level synthesis}
Both our case studies could eventually include an hardware implementation on FPGA. 
Many different CAD software are available on the market for FPGA reconfiguration; as we are leveraging Xilinx FPGAs, we decided to use Xilinx Design Suite: Vivado.\\
Vivado Design Suite is a software produced by Xilinx for synthesis and analysis of HDL designs.
It allows the user to link the software implementation to the the technology that will support it.   %The Vivado Design Suite offers multiple ways to accomplish the tasks involved in Xilinx FPGA design and verification. 
The aim is to generate the bitstream file which will lead to the implementation of the process inside the chosen component. 

\paragraph {Vivado HLS} The first step to be accomplished is Vivado HLS implementation. The process starts by transforming the software code, written with the C or C++ programming language, into RTL (Register Transfer Level)  available in the standard formats VHDL or Verilog. VHDL language is oriented to the circuit description and it is a standard interface among hardware designers.
The output of this step is a core, i.e. a specific representation of a functionality described in VHDL. 

\paragraph {Vivado IP Integrator}
Vivado Design Suite IP integrator (IPI) tool allows to create complex system designs by instantiating and interconnecting IP cores from the Vivado IP catalog into a design canvas. An IP-Core is a core described using a HDL language combined with its communication infrastructure (i.e. the bus interface).
The user can either construct designs at the AXI-interface level for greater productivity, but also manipulate designs at the port level for more precise design control.
So IPI maps RTL components into FPGA resources and connects FPGA resources.

\paragraph {Vivado SDK}  
Next, using Vivado SDK, the primary output of Vivado HLS is synthesized into a bitstream file, which is a file set to configure the FPGA. During this step, Vivado SDK leverages all the development tools needed to create the bitstream file.
As a conclusion, Vivado builds the bridge between the software domain and the hardware domain, providing to the software designers an easy way to speed up the intensive computing part of their algorithm on an FPGA. 




