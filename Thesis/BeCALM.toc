\contentsline {chapter}{\numberline {1}Introduction: Classification purpose}{10}
\contentsline {chapter}{\numberline {2}Literature Review and Problem definition}{12}
\contentsline {section}{\numberline {2.1}The Algorithms}{12}
\contentsline {subsection}{\numberline {2.1.1}Support Vector Machine}{12}
\contentsline {paragraph}{SVM multi-class}{14}
\contentsline {subsection}{\numberline {2.1.2}Artificial Neural Network}{15}
\contentsline {subsection}{\numberline {2.1.3}Convolutional Neural Network}{16}
\contentsline {section}{\numberline {2.2}The role of the FPGA}{20}
\contentsline {subsection}{\numberline {2.2.1}Problem definition}{20}
\contentsline {subsection}{\numberline {2.2.2}The solution: Reconfigurable computing}{20}
\contentsline {subsection}{\numberline {2.2.3}FPGA structure}{23}
\contentsline {subsection}{\numberline {2.2.4}High-level synthesis}{24}
\contentsline {paragraph}{Vivado HLS}{24}
\contentsline {paragraph}{Vivado IP Integrator}{24}
\contentsline {paragraph}{Vivado SDK}{24}
\contentsline {chapter}{\numberline {3}State of Art and Related Work}{26}
\contentsline {section}{\numberline {3.1}Gesture Recognition through Support Vector Machine}{26}
\contentsline {subsection}{\numberline {3.1.1}Support Vector Machine on FPGA}{30}
\contentsline {section}{\numberline {3.2}Diabetic Retinopathy detection through Convolutional Neural Network}{31}
\contentsline {paragraph}{The classical approach}{32}
\contentsline {paragraph}{Deep learning approach: CNN application to Diabetic Retinopathy}{33}
\contentsline {subparagraph}{Diabetic Retinopathy severity classification}{33}
\contentsline {subparagraph}{Lesion Segmentation}{34}
\contentsline {paragraph}{Classical and Deep learning approach: a comparison}{34}
\contentsline {subsection}{\numberline {3.2.1}Artificial Neural Network on FPGA}{35}
\contentsline {subsection}{\numberline {3.2.2}Convolutional Neural Network on FPGA}{36}
\contentsline {chapter}{\numberline {4}Materials and Methods}{39}
\contentsline {section}{\numberline {4.1}Dataset}{39}
\contentsline {paragraph}{EMG Signals}{39}
\contentsline {paragraph}{Retinal Images from Fundus Camera}{40}
\contentsline {section}{\numberline {4.2}Case Study 1: Gesture Recognition through SVM}{41}
\contentsline {subsection}{\numberline {4.2.1}Software Implementation}{41}
\contentsline {subsection}{\numberline {4.2.2}Hardware Implementation}{42}
\contentsline {section}{\numberline {4.3}Case Study 2: Diabetic Retinopathy detection through CNN}{44}
\contentsline {subsection}{\numberline {4.3.1}Dataset elaboration}{44}
\contentsline {subsection}{\numberline {4.3.2}Software Implementation}{45}
\contentsline {paragraph}{Classification procedure}{45}
\contentsline {paragraph}{Training procedure}{47}
\contentsline {chapter}{\numberline {5}Results}{50}
\contentsline {paragraph}{Gesture Recognition}{50}
\contentsline {paragraph}{Microaneurisms Detection}{51}
\contentsline {paragraph}{Cross-entropy Loss:}{51}
\contentsline {paragraph}{Accuracy:}{51}
\contentsline {paragraph}{Time:}{52}
\contentsline {paragraph}{FROC:}{52}
\contentsline {chapter}{\numberline {6}Discussion and Conclusion}{54}
\contentsline {section}{\numberline {6.1}Further improvements and Future works}{54}
\contentsline {paragraph}{Gesture Recognition}{55}
\contentsline {paragraph}{Microaneurisms Detection}{56}
\contentsline {subparagraph}{Dataset:}{56}
\contentsline {subparagraph}{Cropping:}{56}
\contentsline {subparagraph}{Evaluation}{56}
\contentsline {subparagraph}{Network}{57}
\contentsline {subparagraph}{Hardware}{57}
\contentsline {section}{\numberline {6.2}Final Considerations}{57}
